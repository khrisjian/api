//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req,res){
  res.sendFile(path.join(__dirname,'index.html'));
})

app.post("/",function(req,res){
  res.send("Hemos recibido su peticion post");
})

app.get("/clientes/:idcliente",function(req,res){
  var mijson = "{'idcliente':12345}"
  //res.send("Aqui tienes al cliente numero : "+ req.params.idcliente);
  res.send(mijson);
})

app.put("/",function(req,res){
  res.send("Hemos recibido su peticion put cambiada");

})

app.delete("/",function(req,res){
  res.send("Hemos recibido su peticion delete");
})

app.get("/v1/movimientos",function(req,res){
  res.sendFile(path.join(__dirname,'movimientosv1.json'));
})

var movimientosJSON = require('./movimientosv2.json');

app.get("/v2/movimientos",function(req,res){
  res.json(movimientosJSON);
})

app.get("/v2/movimientos/:id",function(req,res){
  console.log(req.params.id);
  res.send(movimientosJSON[req.params.id-1]);// -1 porq los arreglos inician en 0 :D

})

app.get("/v2/movimientosq",function(req,res){
  console.log(req.query);
  res.send("recibido");
})

var bodyparser = require('body-parser');
app.use(bodyparser.json());

app.post('/v2/movimientos',function(req,res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send("Movimientos dado de alta");
})
